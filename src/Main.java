
import java.util.Scanner;

public class Main implements Runnable {
    public static final String ANSI_BLUE = "\033[1;95m";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String GREEN_BOLD_BRIGHT = "\033[1;92m";

    String message;

    Main(String message) {
        this.message = message;
    }

    public void run() {
        try {

            char[] chars = message.toCharArray();

            for (int i = 0; i < chars.length; i++) {
                System.out.print(chars[i]);
                Thread.sleep(300);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean exist = false;
        do {
            System.out.print(GREEN_BOLD_BRIGHT + "->" + ANSI_BLUE + "    Greeting  " + ANSI_RESET);
            String s = sc.nextLine();

            if (s.equals("java Greeting")) {
                try {
                    Thread t1 = new Thread(new Main("Hello KSHRD!"));
                    t1.start();
                    t1.join();
                    System.out.println();
                    Thread t2 = new Thread(new Main("*************************************"));
                    t2.start();
                    t2.join();
                    System.out.println();
                    Thread t3 = new Thread(new Main("I will try my best to be here at HRD."));
                    t3.start();
                    t3.join();
                    System.out.println();
                    Thread t4 = new Thread(new Main("-------------------------------------"));
                    t4.start();
                    t4.join();
                    System.out.println();
                    System.out.print("Downloading");
                    Thread t5 = new Thread(new Main("..........."));
                    t5.start();
                    t5.join();
                    System.out.print("Completed 100%");
                    System.out.println();
                } catch (Exception e) {
                    System.out.println(e);
                }
            } else if (s.equals("stop")) {
                exist = true;
            } else {
                System.out.println("Error: Could not find or load main class greeting");
            }
        } while (exist == false);
    }
}
